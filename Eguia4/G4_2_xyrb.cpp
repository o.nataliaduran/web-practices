#include <stdio.h> 
#include <conio.h>
#include <windows.h> //Libreria para sleep 
#include <math.h> //Libreria para operaciones matematicas complejas

//las variables
float y,r,b,x;

main()
{
	printf("Ingrese el valor de y:\n");
	scanf("%f",&y);
	printf("Ingrese el valor de r:\n");
	scanf("%f",&r);
	printf("Ingrese el valor de b:\n");
	scanf("%f",&b);
	if(b<=0){
		printf("b no puede ser 0 o negativo");
	} else {
		x=y-r;
		printf("Y - R es igual a:%f\n",x);
		if(x<0) {
			printf("La raiz cuadrada de un numero negativo no existe");
		} else {
			x=sqrt(x)/b;
			printf("x es igual a:%f\n",x);
		}//fin de la validacion del calculo de la raiz cuadrada	
	}//fin de la validacion de b
}
